<?php

namespace App\Application;

interface WritingInterface
{
    public function saveNewWriting($user,$writing);
    public function getLastWritings();
    public function getWritingById($id);
}