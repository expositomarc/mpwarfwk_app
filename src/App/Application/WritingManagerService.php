<?php

namespace App\Application;

use Mpwarfwk\Component\Database\SQL;



class WritingManagerService implements WritingInterface {

    private $sql_connection;

    public function __construct () {

        require __DIR__ . '/../../Config/database.php'; // File with database configuration

        // Connect to database with credentials
        $this->sql_connection = new SQL(
                                        $config['SQL']['driver'],
                                        $config['SQL']['host'],
                                        $config['SQL']['database'],
                                        $config['SQL']['username'],
                                        $config['SQL']['password']);
    }


    public function saveNewWriting($user,$writing) {

        // Create array with information to store
        $data = array();
        $data['username'] = $user;
        $data['writing'] = $writing;

        // Insert into database
        $this->sql_connection->insertIntoTableWithData("writings",$data);

    }

    public function getLastWritings() {

        $query = "SELECT id,username,writing FROM writings";

        return $this->sql_connection->selectAllWithQuery($query);

    }


    public function getWritingById($id) {

        // Create array with information to compare in the SELECT statement
        $data = array();
        $data['id'] = $id;

        $query = "SELECT id,username,writing FROM writings WHERE id = :id";

        return $this->sql_connection->selectWithQueryAndData($query,$data);


    }
}