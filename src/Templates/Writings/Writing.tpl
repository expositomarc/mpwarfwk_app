<html>

<head>
    <title>Write something. A simple app using the PHP @expositomarc's framework</title>

    <style>

        body {
            margin:0px;
            font-family: "Helvetica Neue", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
        }
        header {
            width:100%;
            height: 50px;
            background: red;


        }

        main {
            width:100%;
            height: 600px;
            background: white;
            padding-left:5%;
            padding-right:5%;
            box-sizing:border-box;
        }
        footer {
            width:100%;
            height: 50px;
            background: black;
            padding-top: 5px;
        }

        header ul {
            list-style: none;
            background: red;
        }

        header ul li {
            margin-top: 20px;
            display:inline-block;

        }
        header ul a {
            text-decoration: none;
            margin-left: 20px;
        }

        .header-left {
            width:40%;
            height:50px;
            float:left;
            margin-left: 5%;
        }

        .header-right{
            margin-left: 55%;
        }


        .header-left h1 {
            font-size:20px;
        }

        .header-left a {
            text-decoration: none;
            cursor:pointer;
        }

        footer p{
            color:white;
            text-align:center;
        }

        footer a{
            text-decoration: none;
            cursor:pointer;
            color:white;
            font-weight: bold;

        }

        .main-left{
            width:50%;
            float:left;
        }

        .main-right{
            width:50%;
            float:left;
        }


    </style>
</head>

<body>


<header>

    <div class="header-left">
        <h1><a href="/">Write something</a></h1>
    </div>

    <div class="header-right">

        <nav>
            <ul>
                <li><a href="/about-framework">About the framework</a></li>

                <li><a href="/other">Other</a></li>
            </ul>

        </nav>

    </div>

</header>

<main>

    <h2>Write something. A simple app using the PHP @expositomarc's framework</h2>

    <h3>Written by {$username}</h3>

    <p>{$writing}</p>

    <a href="/">Back to the homepage</a>

</main>

<footer>
    <p>This is an example of a project using the PHP @expositomarc's framework for the MPWAR master at La Salle Campus Barcelona. The repository can be found <a href="https://bitbucket.org/expositomarc/mpwarfwk" target="_blank">here</a></p>
</footer>

</body>

</html>