<html>

<head>
    <title>Write something. A simple app using the PHP @expositomarc's framework</title>

    <style>

        body {
            margin:0px;
            font-family: "Helvetica Neue", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
        }
        header {
            width:100%;
            height: 50px;
            background: red;


        }

        main {
            width:100%;
            height: 600px;
            background: white;
            padding-left:5%;
            padding-right:5%;
            box-sizing:border-box;
        }
        footer {
            width:100%;
            height: 50px;
            background: black;
            padding-top: 5px;
        }

        header ul {
            list-style: none;
            background: red;
        }

        header ul li {
            margin-top: 20px;
            display:inline-block;

        }
        header ul a {
            text-decoration: none;
            margin-left: 20px;
        }

        .header-left {
            width:40%;
            height:50px;
            float:left;
            margin-left: 5%;
        }

        .header-right{
            margin-left: 55%;
        }


        .header-left h1 {
            font-size:20px;
        }

        .header-left a {
            text-decoration: none;
            cursor:pointer;
        }

        footer p{
            color:white;
            text-align:center;
        }

        footer a{
            text-decoration: none;
            cursor:pointer;
            color:white;
            font-weight: bold;

        }

        .main-left{
            width:50%;
            float:left;
        }

        .main-right{
            width:50%;
            float:left;
        }


    </style>
</head>

<body>

<header>

    <div class="header-left">
        <h1><a href="/">Write something</a></h1>
    </div>

    <div class="header-right">

        <nav>
            <ul>
                <li><a href="/about-framework">About the framework</a></li>

                <li><a href="/other">Other</a></li>
            </ul>

        </nav>

    </div>

</header>

<main>

    <h1>About the framework</h1>

    <p>Lorem fistrum te va a hasé pupitaa torpedo a wan. A wan ese que llega te va a hasé pupitaa pecador por la gloria de mi madre a peich fistro no te digo trigo por no llamarte Rodrigor. Hasta luego Lucas a wan condemor sexuarl hasta luego Lucas apetecan llevame al sircoo. Ese hombree apetecan ese pedazo de a peich no puedor no te digo trigo por no llamarte Rodrigor qué dise usteer no te digo trigo por no llamarte Rodrigor qué dise usteer me cago en tus muelas torpedo. Ese pedazo de a wan por la gloria de mi madre de la pradera ahorarr ahorarr apetecan. Diodeno va usté muy cargadoo ese pedazo de se calle ustée a gramenawer no te digo trigo por no llamarte Rodrigor va usté muy cargadoo sexuarl por la gloria de mi madre. </p>
    <p>A peich se calle ustée no puedor diodeno benemeritaar diodeno va usté muy cargadoo pecador diodeno. La caidita amatomaa hasta luego Lucas quietooor. Qué dise usteer se calle ustée diodenoo de la pradera pupita de la pradera papaar papaar jarl ese hombree. Torpedo no puedor pupita sexuarl la caidita me cago en tus muelas jarl. Por la gloria de mi madre amatomaa la caidita ese que llega se calle ustée ese que llega caballo blanco caballo negroorl por la gloria de mi madre te voy a borrar el cerito benemeritaar. Benemeritaar a peich qué dise usteer a peich papaar papaar al ataquerl tiene musho peligro jarl apetecan a peich. A wan benemeritaar ese que llega de la pradera a peich apetecan. Te va a hasé pupitaa ese hombree papaar papaar a gramenawer torpedo hasta luego Lucas papaar papaar fistro jarl ese que llega. Pecador me cago en tus muelas se calle ustée no te digo trigo por no llamarte Rodrigor tiene musho peligro. </p>
    <p>Se calle ustée apetecan ahorarr a peich a gramenawer va usté muy cargadoo. Se calle ustée papaar papaar tiene musho peligro condemor ese hombree. La caidita la caidita a wan ese que llega te va a hasé pupitaa hasta luego Lucas benemeritaar. Pupita de la pradera caballo blanco caballo negroorl pupita torpedo diodenoo. Llevame al sircoo mamaar llevame al sircoo diodenoo apetecan está la cosa muy malar pupita va usté muy cargadoo está la cosa muy malar torpedo de la pradera. Se calle ustée pecador te va a hasé pupitaa caballo blanco caballo negroorl. Va usté muy cargadoo papaar papaar a wan por la gloria de mi madre qué dise usteer hasta luego Lucas. Diodenoo condemor torpedo va usté muy cargadoo qué dise usteer ese pedazo de qué dise usteer benemeritaar torpedo pecador pecador. Qué dise usteer mamaar va usté muy cargadoo ese pedazo de me cago en tus muelas se calle ustée pupita te voy a borrar el cerito me cago en tus muelas. </p>


</main>

<footer>
    <p>This is an example of a project using the PHP @expositomarc's framework for the MPWAR master at La Salle Campus Barcelona. The repository can be found <a href="https://bitbucket.org/expositomarc/mpwarfwk" target="_blank">here</a></p>
</footer>

</body>

</html>