<?php

/*
    File configuration with the routes and its controller and method
*/


$config ['routes'] = array(

        array(

            'url' => '/',
                'controller' => 'Controller\\Home\\Welcome',
                'method' => 'mainAction'

        ),

        array(

            'url' => 'about-framework',
            'controller' => 'Controller\\MenuSections\\About',
            'method' => 'mainAction'

        ),

        array(

            'url' => 'other',
            'controller' => 'Controller\\MenuSections\\Other',
            'method' => 'mainAction'

        ),


        array(

            'url' => 'new-writing',
                'controller' => 'Controller\\Writings\\NewWriting',
                'method' => 'mainAction'

        ),

         array(

            'url' => 'writing',
            'controller' => 'Controller\\Writings\\Writing',
            'method' => 'mainAction'

        )

        /*
          Your routes below
          array(

            'url' => 'example',
            'controller' => 'Controller\\Example\\ExampleController',
            'method' => 'exampleMethod'

        )
        */

);

