<?php

/*
    File configuration with the services for the container
*/

$config['writingService'] = array(

            'controller' => 'App\Application\WritingManagerService'

);

/*
 Your services definitions below

$config ['exampleService'] =
    array(
        'controller' => 'App\Application\ExampleService',
        'arguments' => array (
            'App\Application\ExampleService'
    ));
*/
