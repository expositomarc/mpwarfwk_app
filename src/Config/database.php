<?php

/*
    File configuration with the database configurations. In this case, we just use SQL
*/

$config['SQL'] = array(

    'driver' => 'mysql',
    'host' => 'localhost',
    'database' => 'writesomething',
    'username' => 'root',
    'password' => 'strongpassword'

);

