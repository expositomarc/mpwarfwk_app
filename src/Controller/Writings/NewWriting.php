<?php

namespace Controller\Writings;

use Mpwarfwk\Component\Request\Request;
use Mpwarfwk\Component\Container\Container;

class NewWriting
{
    public function __construct()
    {

    }

    public function build()
    {
        echo "NewWriting Controller";
    }

    public function mainAction(Request $request){


        // Add header with redirection to homepage
        header('Location: /');

        // Get writing information from request
        $writingInformation = $this->getWritingInformation($request);

        // Save writing into database
        $this->saveWriting($writingInformation['user'],$writingInformation['writing']);

        return "redirect";
    }

    public function getWritingInformation(Request $request){

        // Get user values
        $user = $request->post->getValue("user");
        $writing = $request->post->getValue("writing");

        $data = array (
            'user'  => $user,
            'writing' => $writing
        );

        return $data;

    }

    public function saveWriting($user,$writing) {

        // Save new writings using writingService
        $container = new Container();
        $container->get('writingService')->saveNewWriting($user,$writing);

    }
}