<?php

namespace Controller\Writings;

use Mpwarfwk\Component\Request\Request;
use Mpwarfwk\Component\Response\Response;
use Mpwarfwk\Component\Templating\SmartyTemplate;
use Mpwarfwk\Component\Container\Container;

class Writing
{
    private $writingTemplate;

    public function __construct()
    {
        $this->writingTemplate = dirname( __FILE__ ) . '/../../Templates/Writings/Writing.tpl';
    }

    public function build()
    {
        echo "Writing Controller";
    }

    public function mainAction(Request $request){

        // Get id
        $id_request = $this->getIdRequest($request);

        // Get writing with id
        $writing = $this->getWriting($id_request);

        // Get view with info
        $view = $this->instantiateViewWithInfo($writing);

        // Return response
        return new Response($view->render($this->writingTemplate));
    }

    public function getIdRequest(Request $request) {

        $uri = $request->server->getValue( 'REQUEST_URI' );
        $uri_params = explode( '/', $uri );
        $id_request = $uri_params[2];

        return $id_request;
    }

    public function getWriting($id_request) {

        // Get writing by id
        $container = new Container();
        $writing = $container->get('writingService')->getWritingById($id_request);

        return $writing;
    }

    public function instantiateViewWithInfo($writing) {

        // Instantiate template and assign vars
        $view = new SmartyTemplate();

        $info = array (
            'username'=> $writing[0]["username"],
            'writing' => $writing[0]["writing"]
        );

        $view->assignVars($info);

        return $view;
    }
}