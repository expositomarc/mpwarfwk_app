<?php

namespace Controller\MenuSections;

use Mpwarfwk\Component\Request\Request;
use Mpwarfwk\Component\Response\Response;
use Mpwarfwk\Component\Templating\SmartyTemplate;
use Mpwarfwk\Component\Container\Container;

class Other
{
    public function __construct()
    {

    }

    public function build()
    {
        echo "Other Controller";
    }

    public function mainAction(Request $request){

        $view = new SmartyTemplate();
        $response = new Response($view->render(dirname( __FILE__ ) . '/../../Templates/MenuSections/Other.tpl'));

        return $response;
    }
}