<?php

namespace Controller\MenuSections;

use Mpwarfwk\Component\Request\Request;
use Mpwarfwk\Component\Response\Response;
use Mpwarfwk\Component\Templating\SmartyTemplate;
use Mpwarfwk\Component\Container\Container;

class About
{
    public function __construct()
    {

    }

    public function build()
    {
        echo "Welcome Controller";
    }

    public function mainAction(Request $request){

        $view = new SmartyTemplate();
        $response = new Response($view->render(dirname( __FILE__ ) . '/../../Templates/MenuSections/About.tpl'));

        return $response;
    }
}