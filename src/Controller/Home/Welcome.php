<?php

namespace Controller\Home;

use Mpwarfwk\Component\Request\Request;
use Mpwarfwk\Component\Response\Response;
use Mpwarfwk\Component\Templating\SmartyTemplate;
use Mpwarfwk\Component\Container\Container;

class Welcome
{
    private $welcomeTemplate;

    public function __construct()
    {
        $this->welcomeTemplate = dirname( __FILE__ ) . '/../../Templates/Home/Welcome.tpl';
    }

    public function build()
    {
        echo "Welcome Controller";
    }

    public function mainAction(Request $request){

        // Get last writings from database
        $info = $this->getAllWritingsFromDatabase();

        // Create view with info assigned
        $view = $this->instantiateViewWithInfo($info);

        return new Response($view->render($this->welcomeTemplate));
    }

    public function getAllWritingsFromDatabase() {

        // Get last writings from the database
        $container = new Container();
        $lastWritings = $container->get('writingService')->getLastWritings();

        $info = array (

            'username' => $lastWritings
        );

        return $info;
    }

    public function instantiateViewWithInfo($info) {

        // Create and assign vars to template
        $view = new SmartyTemplate();

        $view->assignVars($info);

        return $view;
    }
}