<?php

namespace Controller\Error;

use Mpwarfwk\Component\Request\Request;
use Mpwarfwk\Component\Response\Response;
use Mpwarfwk\Component\Templating\SmartyTemplate;

class Error404
{
    private $errorTemplate;

    public function __construct()
    {
        $this->errorTemplate = dirname( __FILE__ ) . '/../../Templates/Error/Error404.tpl';
    }

    public function build()
    {
        echo "Error 404 Controller";
    }

    public function mainAction(Request $request){

        // Add header with Error 404 Not found
        header("HTTP/1.0 404 Not Found");

        $view = $this->instantiateView();

        return new Response($view->render($this->errorTemplate));
    }

    public function instantiateView() {

        // Show view
        $view = new SmartyTemplate();

        return $view;

    }
}