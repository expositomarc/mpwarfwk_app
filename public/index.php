<?php

    include "../vendor/autoload.php";

    $bootstrap = new \Mpwarfwk\Component\Bootstrap();

    $bootstrap->production = true;

    $response = $bootstrap->execute();

    $response->send();
