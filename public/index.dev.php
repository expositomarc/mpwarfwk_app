<?php

include "../vendor/autoload.php";

$bootstrap = new \Mpwarfwk\Component\Bootstrap();

$bootstrap->production = false;

$response = $bootstrap->execute();

$response->send();
