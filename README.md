# Installation of mpwarfwk_app, a simple app using the Marc Exposito's framework #

First of all, download de mpwarfwk_app to your computer. To do this, type:

	git clone https://expositomarc@bitbucket.org/expositomarc/mpwarfwk_app.git

It will download a folder called "mpwarfwk_app". Enter in the folder.

Then, it is required to execute composer to manage the dependencies. To do this, type:

	php composer.phar install

This will execute the composer.json and it will download the @expositomarc's framework and its dependencies, such as twig and smarty.

If password is required -> Pablo mira el mail amb la contrassenya que t'he passat :)

Now we have our project with our framework ready to use, but we first need to create the virtualhost. To do this, type:

	sudo vim /etc/httpd/conf.d/20-mpwarfwk_app.dev.conf

And type:

	<VirtualHost *:80>
	 DocumentRoot /www/mpwarfwk_app/public
	 ServerName mpwarfwkapp.dev
	 ServerAlias *.mpwarfwkapp.dev
	 RewriteEngine On
	 #Allowed media extensions (includes .txt files for robots or .html, e.g: Google hosted HTMLs):
	 RewriteCond %{REQUEST_FILENAME} !^(.+)\.(js|css|gif|png|jpg|swf|ico|txt|html)$
	 RewriteRule ^/(.+) /index.php [QSA,L]
	</VirtualHost>



IMPORTANT! This line has to be modified to match with your folder's path: 
	
	DocumentRoot /www/mpwarfwk_app/public

This framework supports production and development environments. Just modify this line according to your needs:
	
	RewriteRule ^/(.+) /index.php [QSA,L]


Once we have created our virtualhost properly, we need to set a new line in our hosts file. This file is stored in our computer (no Vagrant) here : /etc/hosts

	sudo vim /etc/hosts

And then set the line. In my case:

192.168.33.15   mpwarfwkapp.dev

Once is done, in Vagrant type:
	sudo /etc/init.d/httpd restart

We have almost finished! We just need to set up our database environment.

First of all, we need to create our database with the tables. Using the **writesomething_dump.sql**, we can create our database. This database will create a simple table called writings with some information.

The last step is to set the credentials for the database. You can set these credentials in the **database.php** file, inside the Config folder of the app. Basically, you have to edit:

$config['SQL'] = array(

    'driver' => 'mysql',
    'host' => 'localhost',
    'database' => 'writesomething',
    'username' => 'root',
    'password' => 'strongpassword'

);

... and that's all! Just enter in mpwarfwkapp.dev and you should see the app working.

